package com.example.testSelenium;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.selenide.AllureSelenide;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import static com.codeborne.selenide.Selenide.$x;
import static org.junit.jupiter.api.Assertions.*;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.lang.*;

public class MainPageTest {
    WebDriver driver;
    MainPage mainPage = new MainPage();
    Map<String, Object[]> resultsTest;
    HSSFWorkbook workbook;
    HSSFSheet sheet;
    DateTimeFormatter dtf = DateTimeFormatter.BASIC_ISO_DATE.ofPattern("dd/MM/yyyy HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    String issue;
    String testFail = "";
    @BeforeAll
    public static void setUpAll() {
        Configuration.browserSize = "1536x864";
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @BeforeClass
    public void setUpBeforeClass() {
        WebDriverManager.chromedriver().setup();
        Configuration.startMaximized = true;
        workbook = new HSSFWorkbook();
        sheet = workbook.createSheet("Result test");
        resultsTest = new LinkedHashMap<String, Object[]>();
        resultsTest.put("1", new Object[] {"Test step No.", "Action","Test case description", "Exected Output", "Date test", "Actual Output", "Issues"});
        try {
        } catch (Exception e) {
            throw new IllformedLocaleException("Can't start");
        }
    }

    @BeforeEach
    void setUp() {
        open("https://www.phptravels.net/");
    }

    @Test (priority=1)
    public void setUpBefore() {
        System.out.println("Hello");
        System.out.println(dtf.format(now));
    }

    @Test (priority=2)
    public void signUp() {
        issue = "=\"\"";
        try {
            open("https://www.phptravels.net/");
            $(".dropdown-login").click();
            $x("//a[@href='https://www.phptravels.net/register']").click();
            $x("//input[@type='text' and @name='firstname']").sendKeys("Huỳnh");
            $x("//input[@type='text' and @name='lastname']").sendKeys("Tín");
            $x("//input[@type='text' and @name='phone']").sendKeys("0123456789");
            $x("//input[@type='text' and @name='email']").sendKeys("huynhtin161@gmail.com");
            $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
            $x("//input[@type='password' and @name='confirmpassword']").sendKeys("123456789tin");
            issue += "&CHAR(10)&\"1. Trường hợp đúng tất cả: Pass\"";
        }catch (AssertionError e) {
            issue += "&CHAR(10)&\"1. Trường hợp đúng tất cả: Fail\"";
            testFail = "Fail";
        }
    }

    @Test (priority=3)
    public void signUpFirstName() {
        try {
            open("https://www.phptravels.net/");
            $(".dropdown-login").click();
            $x("//a[@href='https://www.phptravels.net/register']").click();
            $x("//input[@type='text' and @name='lastname']").sendKeys("Tín");
            $x("//input[@type='text' and @name='phone']").sendKeys("0123456789");
            $x("//input[@type='text' and @name='email']").sendKeys("huynhtin161@gmail.com");
            $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
            $x("//input[@type='password' and @name='confirmpassword']").sendKeys("123456789tin");
            $x("//button[@type='submit']").click();
            issue += "&CHAR(10)&\"2. Trường hợp không nhập First Name: Pass\"";
        }catch (AssertionError e) {
            issue += "&CHAR(10)&\"2. Trường hợp không nhập First Name: Fail\"";
            testFail = "Fail";
        }
    }

    @Test (priority=4)
    public void signUpLastName() {
        try {
            open("https://www.phptravels.net/");
            $(".dropdown-login").click();
            $x("//a[@href='https://www.phptravels.net/register']").click();
            $x("//input[@type='text' and @name='firstname']").sendKeys("Huỳnh");
//            $x("//input[@type='text' and @name='lastname']").sendKeys("Tín");
            $x("//input[@type='text' and @name='phone']").sendKeys("0123456789");
            $x("//input[@type='text' and @name='email']").sendKeys("huynhtin161@gmail.com");
            $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
            $x("//input[@type='password' and @name='confirmpassword']").sendKeys("123456789tin");
            $x("//button[@type='submit']").click();
            issue += "&CHAR(10)&\"3. Trường hợp không nhập Last Name: Pass\"";
        }catch (AssertionError e) {
            issue += "&CHAR(10)&\"3. Trường hợp không nhập Last Name: Fail\"";
            testFail = "Fail";
        }
    }

    @Test (priority=5)
    public void signUpPhone() {
        try {
            open("https://www.phptravels.net/");
            $(".dropdown-login").click();
            $x("//a[@href='https://www.phptravels.net/register']").click();
            $x("//input[@type='text' and @name='firstname']").sendKeys("Huỳnh");
            $x("//input[@type='text' and @name='lastname']").sendKeys("Tín");
//            $x("//input[@type='text' and @name='phone']").sendKeys("0123456789");
            $x("//input[@type='text' and @name='email']").sendKeys("huynhtin161@gmail.com");
            $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
            $x("//input[@type='password' and @name='confirmpassword']").sendKeys("123456789tin");
            $x("//button[@type='submit']").click();
            issue += "&CHAR(10)&\"4. Trường hợp không nhập Phone: Pass\"";
        }catch (AssertionError e) {
            issue += "&CHAR(10)&\"4. Trường hợp không nhập Phone: Fail\"";
            testFail = "Fail";
        }
    }

    @Test (priority=6)
    public void signUpEmail() {
        try {
            open("https://www.phptravels.net/");
            $(".dropdown-login").click();
            $x("//a[@href='https://www.phptravels.net/register']").click();
            $x("//input[@type='text' and @name='firstname']").sendKeys("Huỳnh");
            $x("//input[@type='text' and @name='lastname']").sendKeys("Tín");
            $x("//input[@type='text' and @name='phone']").sendKeys("0123456789");
//            $x("//input[@type='text' and @name='email']").sendKeys("huynhtin161@gmail.com");
            $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
            $x("//input[@type='password' and @name='confirmpassword']").sendKeys("123456789tin");
            $x("//button[@type='submit']").click();
            issue += "&CHAR(10)&\"5. Trường hợp không nhập Email: Pass\"";
        }catch (AssertionError e) {
            issue += "&CHAR(10)&\"5. Trường hợp không nhập Email: Fail\"";
            testFail = "Fail";
        }
    }

    @Test (priority=7)
    public void signUpValidateEmail() {
        try {
            open("https://www.phptravels.net/");
            $(".dropdown-login").click();
            $x("//a[@href='https://www.phptravels.net/register']").click();
            $x("//input[@type='text' and @name='firstname']").sendKeys("Huỳnh");
            $x("//input[@type='text' and @name='lastname']").sendKeys("Tín");
            $x("//input[@type='text' and @name='phone']").sendKeys("0123456789");
            $x("//input[@type='text' and @name='email']").sendKeys("huynhtin161");
            $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
            $x("//input[@type='password' and @name='confirmpassword']").sendKeys("123456789tin");
            $x("//button[@type='submit']").hover();
            $x("//button[@type='submit']").click();
            String alert = "";
            alert = $x("//p[text()='The Email field must contain a valid email address.']").getText();
            if (alert != "") {
                issue += "&CHAR(10)&\"6. Trường hợp validate Email: Pass\"";
            }
        }catch (AssertionError e) {
            issue += "&CHAR(10)&\"6. Trường hợp validate Email: Fail\"";
            testFail = "Fail";
        }
    }

    @Test (priority=8)
    public void signUpPassword() {
        try {
            open("https://www.phptravels.net/");
            $(".dropdown-login").click();
            $x("//a[@href='https://www.phptravels.net/register']").click();
            $x("//input[@type='text' and @name='firstname']").sendKeys("Huỳnh");
            $x("//input[@type='text' and @name='lastname']").sendKeys("Tín");
            $x("//input[@type='text' and @name='phone']").sendKeys("0123456789");
            $x("//input[@type='text' and @name='email']").sendKeys("huynhtin161");
//            $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
            $x("//input[@type='password' and @name='confirmpassword']").sendKeys("123456789tin");
            $x("//button[@type='submit']").hover();
            $x("//button[@type='submit']").click();
            issue += "&CHAR(10)&\"7. Trường hợp không nhập Password: Pass\"";
        }catch (AssertionError e) {
            issue += "&CHAR(10)&\"7. Trường hợp không nhập Password: Fail\"";
            testFail = "Fail";
        }
    }

    @Test (priority=9)
    public void signUpValidatePassword() {
        try {
            open("https://www.phptravels.net/");
            $(".dropdown-login").click();
            $x("//a[@href='https://www.phptravels.net/register']").click();
            $x("//input[@type='text' and @name='firstname']").sendKeys("Huỳnh");
            $x("//input[@type='text' and @name='lastname']").sendKeys("Tín");
            $x("//input[@type='text' and @name='phone']").sendKeys("0123456789");
            $x("//input[@type='text' and @name='email']").sendKeys("huynhtin161");
            $x("//input[@type='password' and @name='password']").sendKeys("123");
            $x("//input[@type='password' and @name='confirmpassword']").sendKeys("123");
            $x("//button[@type='submit']").hover();
            $x("//button[@type='submit']").click();

            String alert = "";
            alert = $x("//p[text()='The Password field must be at least 6 characters in length.']").getText();
            if (alert != "") {
                issue += "&CHAR(10)&\"8. Trường hợp validate Password: Pass\"";
            }
        }catch (AssertionError e) {
            issue += "&CHAR(10)&\"8. Trường hợp validate Password: Fail\"";
            testFail = "Fail";
        }
    }

    @Test (priority=10)
    public void signUpConfirmpassword() {
        try {
            open("https://www.phptravels.net/");
            $(".dropdown-login").click();
            $x("//a[@href='https://www.phptravels.net/register']").click();
            $x("//input[@type='text' and @name='firstname']").sendKeys("Huỳnh");
            $x("//input[@type='text' and @name='lastname']").sendKeys("Tín");
            $x("//input[@type='text' and @name='phone']").sendKeys("0123456789");
            $x("//input[@type='text' and @name='email']").sendKeys("huynhtin161");
            $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
//            $x("//input[@type='password' and @name='confirmpassword']").sendKeys("123456789tin");
            $x("//button[@type='submit']").hover();
            $x("//button[@type='submit']").click();
            issue += "&CHAR(10)&\"9. Trường hợp không nhập Confirm Password: Pass\"";
        }catch (AssertionError e) {
            issue += "&CHAR(10)&\"9. Trường hợp không nhập Confirm Password: Fail\"";
            testFail = "Fail";
        }
    }

    @Test (priority=11)
    public void signUpPasswordNotMatchConfirmpassword() {
        try {
            open("https://www.phptravels.net/");
            $(".dropdown-login").click();
            $x("//a[@href='https://www.phptravels.net/register']").click();
            $x("//input[@type='text' and @name='firstname']").sendKeys("Huỳnh");
            $x("//input[@type='text' and @name='lastname']").sendKeys("Tín");
            $x("//input[@type='text' and @name='phone']").sendKeys("0123456789");
            $x("//input[@type='text' and @name='email']").sendKeys("huynhtin161");
            $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
            $x("//input[@type='password' and @name='confirmpassword']").sendKeys("1234567890tin");
            $x("//button[@type='submit']").hover();
            $x("//button[@type='submit']").click();

            String alert = "";
            alert = $x("//p[text()='Password not matching with confirm password.']").getText();
            if (alert != "") {
                issue += "&CHAR(10)&\"10. Trường hợp Password không khớp với Confirm Password: Pass\"";
            }
        }catch (AssertionError e) {
            issue += "&CHAR(10)&\"10. Trường hợp Password không khớp với Confirm Password: Fail\"";
            testFail = "Fail";
        }
    }

    @Test (priority=12)
    public void printResultSignUp() {
        if (testFail == "") {
            resultsTest.put("2", new Object[] {1d, "Create account", "=\"- Thử tất cả các trường hợp khi tạo tài khoản\"&CHAR(10)&\"- Các trường hợp validate\"", "Output pass", dtf.format(now), "Pass", issue});
        } else {
            resultsTest.put("2", new Object[] {1d, "Create account", "=\"- Thử tất cả các trường hợp khi tạo tài khoản\"&CHAR(10)&\"- Các trường hợp validate\"", "Output pass", dtf.format(now), "Fail", issue});
            Assert.assertTrue(false);
        }
    }

//    @Test (priority=3)
//    public void login() {
//        try {
//            open("https://www.phptravels.net/");
//            $(".dropdown-login").click();
//            $x("//a[@href='https://www.phptravels.net/login']").click();
//            $x("//input[@type='email' and @name='username']").sendKeys("huynhtin13");
//            $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
////        $x("//button[@type='submit']").click();
////            Thread.sleep(1000);
//            resultsTest.put("3", new Object[] {2d, "Login", "Login success", "Pass"});
//        } catch (AssertionError e) {
//            resultsTest.put("3", new Object[] {2d, "Login", "Login success", "Fail"});
//            Assert.assertTrue(false);
//        }
//    }
//
//    @Test (priority=4)
//    public void searchHotelDestination() {
//        try {
//            open("https://www.phptravels.net/");
////        mainPage.searchButton.click();
//
//            $(".typeahead__container").click();
//            $(".select2-focused").sendKeys("Hồ Chí Minh");
//            $x("//span[@class='select2-match' and text()='Ho Chi Minh']").click();
//            $x("//button[@type='submit']").click();
////        $x("//button[@type='submit' and text()='Search']").click();
////        $(".js-search-input").shouldHave(attribute("value", "Selenium"));
////            Thread.sleep(1000);
//            resultsTest.put("4", new Object[] {3d, "Search", "Search hotel destination success", "Pass"});
//        } catch (AssertionError e) {
//            resultsTest.put("4", new Object[] {3d, "Search", "Search hotel destination success", "Fail"});
//            Assert.assertTrue(false);
//        }
//    }
//
//    @Test (priority=5)
//    public void searchFlights() {
//        try {
//            open("https://www.phptravels.net/");
//    //        mainPage.searchButton.click();
//
//            $x("//a[@href='#flights']").click();
//            $x("//span[@class='select2-chosen' and text()='Enter city or airport']").click();
//            $(".select2-focused").sendKeys("Hồ Chí Minh");
//            $x("//span[@class='select2-match' and text()='Ho Chi Minh']").click();
//
//            $x("//span[@class='select2-chosen' and text()='Enter city or airport']").click();
//            $(".select2-focused").sendKeys("Tokyo");
//            $x("//span[@class='select2-match' and text()='Tokyo']").click();
////            Thread.sleep(1000);
//            resultsTest.put("5", new Object[] {4d, "Search", "Search flights success", "Pass"});
//        } catch (AssertionError e) {
//            resultsTest.put("5", new Object[] {4d, "Search", "Search flights success", "Fail"});
//            Assert.assertTrue(false);
//        }
//    }
//
//    @Test (priority=6)
//    public void bookTour() {
//        try {
//            open("https://www.phptravels.net/");
//            $x("//a[@href='https://www.phptravels.net/tours/malasia/legoland/Legoland-Malaysia-Day-Pass?date=01/07/2021&adults=1']").shouldBe(visible);
//            $x("//a[@href='https://www.phptravels.net/tours/malasia/legoland/Legoland-Malaysia-Day-Pass?date=01/07/2021&adults=1']").click();
//            $x("//h3[@class='heading-title']//span[text()='Overview']").hover();
//            $x("//button[@type='submit' and text()='Book Now']").click();
//
//            $x("//input[@name='firstname']").sendKeys("Huỳnh");
//            $x("//input[@name='lastname']").sendKeys("Tín");
//            $x("//input[@name='email']").sendKeys("huynhtin161");
//            $x("//input[@name='confirmemail']").sendKeys("123");
//            $x("//input[@name='phone']").sendKeys("0123456789");
//            $x("//input[@name='address']").sendKeys("123456789tin");
//            $x("//a[@class='chosen-single']").click();
//            $x("//input[@class='chosen-search-input']").sendKeys("vietnam");
//            $x("//li[@data-option-array-index='222']").click();
//            $x("//button[text()='CONFIRM THIS BOOKING']").hover();
//            $x("//span[text()='Guest 1 Name']").click();
//            $x("//input[@name='passport[1][name]']").sendKeys("abcd");
//            $x("//span[text()='Guest 1 Passport No.']").click();
//            $x("//input[@name='passport[1][passportnumber]']").sendKeys("abcd");
//            $x("//span[text()='Age']").click();
//            $x("//input[@name='passport[1][age]']").sendKeys("22");
//            $x("//h3[text()='Get the mobile app!']").hover();
////        $x("//button[text()='CONFIRM THIS BOOKING']").click();
////            Thread.sleep(1000);
//            resultsTest.put("6", new Object[] {5d, "Book tour", "Book tour success", "Pass"});
//        } catch (AssertionError e) {
//            resultsTest.put("6", new Object[] {5d, "Book tour", "Book tour success", "Fail"});
//            Assert.assertTrue(false);
//        }
//    }
//
//    @Test (priority=7)
//    public void Visa() {
//        try {
//            open("https://www.phptravels.net/");
//
//            $x("//*[@id=\"search\"]/div/div/div/div/div/nav/ul/li[7]/a").click();
//            $x("//*[@id=\"visa\"]/div/div/form/div/div/div[1]/div/div[2]/div/a/span").click();
//            $x("//*[@id=\"visa\"]/div/div/form/div/div/div[1]/div/div[2]/div/a").click();
//            $x("//*[@id=\"visa\"]/div/div/form/div/div/div[2]/div/div[2]/div/a/span").click();
//            $x("//*[@id=\"visa\"]/div/div/form/div/div/div[3]/div/div[2]/input").click();
//            $x("//*[@id=\"datepickers-container\"]/div[3]/div/div/div[2]/div[14]").click();
//            $x("//*[@id=\"visa\"]/div/div/form/div/div/div[4]/button").click();
//            $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[1]/div[1]/div/label/input").sendKeys("Truong");
//            $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[1]/div[2]/label/input").sendKeys("Nguyen");
//            $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[2]/div[1]/label/input").sendKeys("truonga8@gmail.com");
//            $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[2]/div[2]/label/input").sendKeys("truonga8@gmail.com");
//            $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[3]/div[1]/label/input").sendKeys("01635138167");
//            $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[3]/div[2]/label/input").sendKeys("2-7-2021");
//            $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[4]/div/div[1]/label/input").click();
//            $x("//*[@id=\"special\"]/div/label/textarea").sendKeys("Hello");
//            $x("//*[@id=\"sub\"]").click();
////            Thread.sleep(1000);
//            resultsTest.put("7", new Object[] {6d, "Visa", "Visa success", "Pass"});
//        } catch (AssertionError e) {
//            resultsTest.put("7", new Object[] {6d, "Visa", "Visa success", "Fail"});
//            Assert.assertTrue(false);
//        }
//
//
//    }
//    @Test (priority=8)
//    public void SearchCar() {
//        try {
//            open("https://www.phptravels.net/");
//
//            $x("//*[@id=\"search\"]/div/div/div/div/div/nav/ul/li[6]/a").click();
//            $x("//*[@id=\"cars\"]/div/div/form/div/div/div[1]/div/div").click();
//            $x("//*[@id=\"select2-drop\"]/div/input").sendKeys("Bangkok");
//            $("#select2-drop > ul > li:nth-child(1) > div > span").click();
//            $x("//*[@id=\"s2id_autogen13\"]/a/span[1]").click();
//            $x("//*[@id=\"select2-drop\"]/div/input").sendKeys("Vietnam");
//            $("#select2-drop > ul > li:nth-child(4) > div").click();
//            $x("//*[@id=\"airDatepickerRange-flight\"]/div[2]/div/div/div/a").click();
//            $x("//*[@id=\"airDatepickerRange-flight\"]/div[2]/div/div/div/a/span").click();
//            $x("//*[@id=\"cars\"]/div/div/form/div/div/div[5]/button").click();
////            Thread.sleep(1000);
//            resultsTest.put("8", new Object[] {7d, "Search", "Search cars success", "Pass"});
//        } catch (AssertionError e) {
//            resultsTest.put("8", new Object[] {7d, "Search", "Search cars success", "Fail"});
//            Assert.fail("Failed", e);
//        }
//    }
//    @Test (priority=9)
//    public void SearchRentals() {
//        try {
//            open("https://www.phptravels.net/");
//
//            $x("//*[@id=\"search\"]/div/div/div/div/div/nav/ul/li[4]/a").click();
//            $x("//*[@id=\"s2id_autogen5\"]").click();
//            $x("//*[@id=\"select2-drop\"]/div/input").sendKeys("Ho Chi Minh");
//            $("#select2-drop > ul > li > ul > li > div > span").click();
//            $x("//*[@id=\"rentaltype_chosen\"]/a").click();
//            $x("//*[@id=\"rentaltype_chosen\"]/div/ul/li[5]").click();
//            $x("//*[@id=\"rentals\"]/div/div/form/div/div/div[3]/div/div/div/div/div/div/div[2]/div/div[2]/div/span/button[1]").click();
//            $x("//*[@id=\"rentals\"]/div/div/form/div/div/div[4]/button").click();
////            Thread.sleep(1000);
//            resultsTest.put("9", new Object[] {8d, "Search", "Search rentals success", "Pass"});
//        } catch (AssertionError e) {
//            resultsTest.put("9", new Object[] {8d, "Search", "Search rentals success", "Fail"});
//            Assert.assertTrue(false);
//        }
//    }
//
//    @Test (priority=10)
//    public void SearchAndBuyTour() {
//        try {
//            open("https://www.phptravels.net/");
//
//            $x("//*[@id=\"search\"]/div/div/div/div/div/nav/ul/li[5]/a").click();
//            $x("//*[@id=\"s2id_autogen19\"]/a/span[1]").click();
//            $("#select2-drop > ul > li > ul > li > div").click();
//            $x("//*[@id=\"tourtype_chosen\"]/a").click();
//            $("#tourtype_chosen > div > ul > li:nth-child(8)").click();
//            $x("//*[@id=\"tours\"]/div/div/form/div/div/div[3]/div/div/div/div/div/div/div[2]/div/div[2]/div/span/button[1]").click();
//            $x("//*[@id=\"tours\"]/div/div/form/div/div/div[4]/button").click();
//            $("#packages > tbody > tr:nth-child(2) > td:nth-child(6) > a").click();
//            $x("//*[@id=\"send_enquery\"]/div/div[2]/div[1]/div[1]/div/input[1]").sendKeys("Trường");
//            $x("//*[@id=\"send_enquery\"]/div/div[2]/div[1]/div[2]/div/input").sendKeys("truonga8@gmail.com");
//            $x("//*[@id=\"send_enquery\"]/div/div[2]/div[2]/div[1]/div/input").sendKeys("0123456789");
//            $x("//*[@id=\"send_enquery\"]/div/div[2]/div[2]/div[2]/div/input").sendKeys("Số 17 Trường Xa,Phường 21,Quận Bình Thạnh,Thành Phố Hồ Chí Minh");
//            $x("//*[@id=\"ClickMyButton\"]").click();
////            Thread.sleep(1000);
//            resultsTest.put("10", new Object[] {9d, "Search", "Search and buy tour success", "Pass"});
//        } catch (AssertionError e) {
//            resultsTest.put("10", new Object[] {9d, "Search", "Search and buy tour success", "Fail"});
//            Assert.assertTrue(false);
//        }
//    }

    @AfterClass
    public void Finish() {
        System.out.println("Kết thúc");
        Set<String> keyset = resultsTest.keySet();
        int rowNum = 0;
        for (String key : keyset) {
            Row row = sheet.createRow(rowNum++);
            Object[] objArr = resultsTest.get(key);
            int cellNum = 0;
            for (Object obj : objArr) {
                Cell cell = row.createCell(cellNum++);
                if (obj instanceof Date) {
                    cell.setCellValue((Date) obj);
                } else if (obj instanceof Boolean) {
                    cell.setCellValue((Boolean) obj);
                } else if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {
            FileOutputStream out = new FileOutputStream(new File("ResultTestToExCel.xls"));
            workbook.write(out);
            out.close();
            System.out.println("In file excel thành công!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
