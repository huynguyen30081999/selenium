package com.example.testSelenium;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import static com.codeborne.selenide.Selenide.$x;
import static org.junit.jupiter.api.Assertions.*;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

public class MainPageTestUpdate {
    MainPage mainPage = new MainPage();

    @BeforeAll
    public static void setUpAll() {
        Configuration.browserSize = "1536x864";
        SelenideLogger.addListener("allure", new AllureSelenide());
    }

    @BeforeEach
    public void setUp() {
        open("https://www.phptravels.net/");
    }

    @Test
    public void shouldAnswerWidthTrue() {
        System.out.println("Hello");
        assertTrue(true);
    }

    @Test
    public void signUp() {
        $(".dropdown-login").click();
        $x("//a[@href='https://www.phptravels.net/register']").click();
        $x("//input[@type='text' and @name='firstname']").sendKeys("Huỳnh");
        $x("//input[@type='text' and @name='lastname']").sendKeys("Tín");
        $x("//input[@type='text' and @name='phone']").sendKeys("0123456789");
        $x("//input[@type='text' and @name='email']").sendKeys("huynhtin161@gmail.com");
        $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
        $x("//input[@type='password' and @name='confirmpassword']").sendKeys("123456789tin");
//        $x("//button[@type='submit']").click();
    }

    @Test
    public void login() {
        $(".dropdown-login").click();
        $x("//a[@href='https://www.phptravels.net/login']").click();
        $x("//input[@type='email' and @name='username']").sendKeys("huynhtin13");
        $x("//input[@type='password' and @name='password']").sendKeys("123456789tin");
//        $x("//button[@type='submit']").click();
    }

    @Test
    public void searchHotelDestination() {
//        mainPage.searchButton.click();

        $(".typeahead__container").click();
        $(".select2-focused").sendKeys("Hồ Chí Minh");
        $x("//span[@class='select2-match' and text()='Ho Chi Minh']").click();
        $x("//button[@type='submit']").click();
//        $x("//button[@type='submit' and text()='Search']").click();
//        $(".js-search-input").shouldHave(attribute("value", "Selenium"));
    }

    @Test
    public void searchFlights() {
//        mainPage.searchButton.click();

        $x("//a[@href='#flights']").click();
        $x("//span[@class='select2-chosen' and text()='Enter city or airport']").click();
        $(".select2-focused").sendKeys("Hồ Chí Minh");
        $x("//span[@class='select2-match' and text()='Ho Chi Minh']").click();

        $x("//span[@class='select2-chosen' and text()='Enter city or airport']").click();
        $(".select2-focused").sendKeys("Tokyo");
        $x("//span[@class='select2-match' and text()='Tokyo']").click();
    }

    @Test
    public void bookTour() {
        $x("//a[@href='https://www.phptravels.net/tours/malasia/legoland/Legoland-Malaysia-Day-Pass?date=01/07/2021&adults=1']").shouldBe(visible);
        $x("//a[@href='https://www.phptravels.net/tours/malasia/legoland/Legoland-Malaysia-Day-Pass?date=01/07/2021&adults=1']").click();
        $x("//h3[@class='heading-title']//span[text()='Overview']").hover();
        $x("//button[@type='submit' and text()='Book Now']").click();

        $x("//input[@name='firstname']").sendKeys("Huỳnh");
        $x("//input[@name='lastname']").sendKeys("Tín");
        $x("//input[@name='email']").sendKeys("huynhtin161");
        $x("//input[@name='confirmemail']").sendKeys("123");
        $x("//input[@name='phone']").sendKeys("0123456789");
        $x("//input[@name='address']").sendKeys("123456789tin");
        $x("//a[@class='chosen-single']").click();
        $x("//input[@class='chosen-search-input']").sendKeys("vietnam");
        $x("//li[@data-option-array-index='222']").click();
        $x("//button[text()='CONFIRM THIS BOOKING']").hover();
        $x("//span[text()='Guest 1 Name']").click();
        $x("//input[@name='passport[1][name]']").sendKeys("abcd");
        $x("//span[text()='Guest 1 Passport No.']").click();
        $x("//input[@name='passport[1][passportnumber]']").sendKeys("abcd");
        $x("//span[text()='Age']").click();
        $x("//input[@name='passport[1][age]']").sendKeys("22");
        $x("//h3[text()='Get the mobile app!']").hover();
//        $x("//button[text()='CONFIRM THIS BOOKING']").click();
    }

//    @Test
//    public void toolsMenu() {
//        mainPage.toolsMenu.hover();
//
//        $x("//a[@class='sf-with-ul' and text()='Women']").shouldBe(visible);
//    }

    //    @Test
//    public void navigationToAllTools() {
//        mainPage.seeAllToolsButton.click();
//
//        $(".products-list").shouldBe(visible);
//
//        assertEquals("All Developer Tools and Products by JetBrains", Selenide.title());
//    }
    @Test
    public void Visa() {

        $x("//*[@id=\"search\"]/div/div/div/div/div/nav/ul/li[7]/a").click();
        $x("//*[@id=\"visa\"]/div/div/form/div/div/div[1]/div/div[2]/div/a/span").click();
        $x("//*[@id=\"visa\"]/div/div/form/div/div/div[1]/div/div[2]/div/a").click();
        $x("//*[@id=\"visa\"]/div/div/form/div/div/div[2]/div/div[2]/div/a/span").click();
        $x("//*[@id=\"visa\"]/div/div/form/div/div/div[3]/div/div[2]/input").click();
        $x("//*[@id=\"datepickers-container\"]/div[3]/div/div/div[2]/div[14]").click();
        $x("//*[@id=\"visa\"]/div/div/form/div/div/div[4]/button").click();
        $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[1]/div[1]/div/label/input").sendKeys("Truong");
        $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[1]/div[2]/label/input").sendKeys("Nguyen");
        $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[2]/div[1]/label/input").sendKeys("truonga8@gmail.com");
        $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[2]/div[2]/label/input").sendKeys("truonga8@gmail.com");
        $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[3]/div[1]/label/input").sendKeys("01635138167");
        $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[3]/div[2]/label/input").sendKeys("2-7-2021");
        $x("//*[@id=\"toggleStyle04-collapseOne\"]/div/div/form/div[4]/div/div[1]/label/input").click();
        $x("//*[@id=\"special\"]/div/label/textarea").sendKeys("Hello");
        $x("//*[@id=\"sub\"]").click();
    }
    @Test
    public void SearchCar(){
        $x("//*[@id=\"search\"]/div/div/div/div/div/nav/ul/li[6]/a").click();
        $x("//*[@id=\"cars\"]/div/div/form/div/div/div[1]/div/div").click();
        $x("//*[@id=\"select2-drop\"]/div/input").sendKeys("Bangkok");
        $("#select2-drop > ul > li:nth-child(1) > div > span").click();
        $x("//*[@id=\"s2id_autogen13\"]/a/span[1]").click();
        $x("//*[@id=\"select2-drop\"]/div/input").sendKeys("Vietnam");
        $("#select2-drop > ul > li:nth-child(4) > div").click();
        $x("//*[@id=\"airDatepickerRange-flight\"]/div[2]/div/div/div/a").click();
        $x("//*[@id=\"airDatepickerRange-flight\"]/div[2]/div/div/div/a/span").click();
        $x("//*[@id=\"cars\"]/div/div/form/div/div/div[5]/button").click();
    }
    @Test
    public void SearchRentals(){
        $x("//*[@id=\"search\"]/div/div/div/div/div/nav/ul/li[4]/a").click();
        $x("//*[@id=\"s2id_autogen5\"]").click();
        $x("//*[@id=\"select2-drop\"]/div/input").sendKeys("Ho Chi Minh");
        $("#select2-drop > ul > li > ul > li > div > span").click();
        $x("//*[@id=\"rentaltype_chosen\"]/a").click();
        $x("//*[@id=\"rentaltype_chosen\"]/div/ul/li[5]").click();
        $x("//*[@id=\"rentals\"]/div/div/form/div/div/div[3]/div/div/div/div/div/div/div[2]/div/div[2]/div/span/button[1]").click();
        $x("//*[@id=\"rentals\"]/div/div/form/div/div/div[4]/button").click();
    }

    @Test
    public void SearchAndBuyTour(){
        $x("//*[@id=\"search\"]/div/div/div/div/div/nav/ul/li[5]/a").click();
        $x("//*[@id=\"s2id_autogen19\"]/a/span[1]").click();
        $("#select2-drop > ul > li > ul > li > div").click();
        $x("//*[@id=\"tourtype_chosen\"]/a").click();
        $("#tourtype_chosen > div > ul > li:nth-child(8)").click();
        $x("//*[@id=\"tours\"]/div/div/form/div/div/div[3]/div/div/div/div/div/div/div[2]/div/div[2]/div/span/button[1]").click();
        $x("//*[@id=\"tours\"]/div/div/form/div/div/div[4]/button").click();
        $("#packages > tbody > tr:nth-child(2) > td:nth-child(6) > a").click();
        $x("//*[@id=\"send_enquery\"]/div/div[2]/div[1]/div[1]/div/input[1]").sendKeys("Trường");
        $x("//*[@id=\"send_enquery\"]/div/div[2]/div[1]/div[2]/div/input").sendKeys("truonga8@gmail.com");
        $x("//*[@id=\"send_enquery\"]/div/div[2]/div[2]/div[1]/div/input").sendKeys("0123456789");
        $x("//*[@id=\"send_enquery\"]/div/div[2]/div[2]/div[2]/div/input").sendKeys("Số 17 Trường Xa,Phường 21,Quận Bình Thạnh,Thành Phố Hồ Chí Minh");
        $x("//*[@id=\"ClickMyButton\"]").click();
    }

    @AfterAll
    public static void Finish() {
        System.out.println("Kết thúc");
    }
}
